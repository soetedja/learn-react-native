import React from 'react';
import { StyleSheet, View, Text, PanResponder } from 'react-native';

export default class GesturesScreen extends React.Component {
  
  state = {
    dragging: false,
    progressY: 50,
    progressX: 50,
    y: 50,
    x: 50,
    offsetTop: 0,
    offsetLeft: 0,
  }


  panResponder = {}

  UNSAFE_componentWillMount () {
    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: this.handleStartShouldSetPanResponder,
      onPanResponderGrant: this.handlePanResponderGrant,
      onPanResponderMove: this.handlePanResponderMove,
      onPanResponderRelease: this.handlePanResponderEnd,
      onPanResponderTerminate: this.handlePanResponderEnd
    });
  }

  render () {
    const {dragging, y, x, progressX, progressY, offsetTop, offsetLeft} = this.state;

    // Update style with the state of the drag thus far
    const style = {
      backgroundColor: dragging ? 'skyblue' : 'steelblue',
      top: y + offsetTop,
      left: x + offsetLeft,
    };

    return (
      <View style={styles.container}>
        <View style={styles.summary}>
          <Text>X: {x}</Text>
          <Text>Y: {y}</Text>
          <Text>Progess X: {progressX}</Text>
          <Text>Progress Y: {progressY}</Text>
        </View>
        <View
          // Put all panHandlers into the View's props
          {...this.panResponder.panHandlers}
          style={[styles.square, style]}
        >
          <Text style={styles.text}>
            DRAG ME
          </Text>
        </View>
      </View>
    );
  }
  
  // Should we become active when the user presses down on the square?
  handleStartShouldSetPanResponder = () => {
    return true;
  }

  // We were granted responder status! Let's update the UI
  handlePanResponderGrant = () => {
    this.setState({dragging: true});
  }

  // Every time the touch/mouse moves
  handlePanResponderMove = (e, gestureState) => {
    const {x,y} = this.state;
    // Keep track of how far we've moved in total (dx and dy)
    this.setState({
      offsetTop: gestureState.dy,
      offsetLeft: gestureState.dx,
      progressY: y + gestureState.dy,
      progressX: x + gestureState.dx
    });
  }

  // When the touch/mouse is lifted
  handlePanResponderEnd = (e, gestureState) => {
    const {y, x} = this.state;

    // The drag is finished. Set the y and x so that
    // the new position sticks. Reset offsetTop and offsetLeft for the next drag.
    this.setState({
      dragging: false,
      y: y + gestureState.dy,
      x: x + gestureState.dx,
      offsetTop: 0,
      offsetLeft: 0,
    });
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  summary:{
    padding: 5,
  },
  square: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: 80,
    height: 80,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: 'white',
    fontSize: 12,
  }
});
