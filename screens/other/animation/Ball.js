import React from 'react';
import { View , Animated} from 'react-native';

export default class Ball extends React.Component {
  
   position = new Animated.ValueXY(0, 0);

   getRandomInt(min, max) {
     min = Math.ceil(min);
     max = Math.floor(max);
     console.log(Math.floor(Math.random() * (max - min)) + min);
     return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
   }

   randomPosition(run) {
     setTimeout(() => {
       Animated.spring(this.position, {
         toValue: { x: this.getRandomInt(0, 300), y: this.getRandomInt(0, 300)}
       }).start();
       this.randomPosition(run);
     }, 1000);
   }

   UNSAFE_componentWillMount() {
     this.randomPosition(); 
   }
   
   componentWillUnmount(){
     this.randomPosition(false);
   }
  
   render() {
     return (
       <Animated.View style={this.position.getLayout()}>
         <View style={styles.ball} />
       </Animated.View>
     );
   }
}

const styles = {
  ball: {
    height: 60,
    width: 60,
    borderRadius: 30,
    borderWidth: 30,
    borderColor: 'black'
  }
};