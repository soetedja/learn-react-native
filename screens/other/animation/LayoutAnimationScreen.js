import React from 'react';
import {
  NativeModules,
  LayoutAnimation,
  Text,
  TouchableOpacity,
  StyleSheet,
  View,
} from 'react-native';

const { UIManager } = NativeModules;

UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);

export default class LayoutAnimationScreen extends React.Component {
  state = {
    w: 10,
    h: 10,
    w2: 5,
    h2: 5,
  };

  _onPress = () => {
    // Animate the update
    LayoutAnimation.spring();
    this.setState({
      w: this.state.w + 15, 
      h: this.state.h + 15,
      w2: this.state.w2 + 50,
      h2: this.state.h2 + 50
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={[styles.box, {width: this.state.w, height: this.state.h}]} />
        <View style={[styles.box2, {width: this.state.w2, height: this.state.h2}]} />
        <TouchableOpacity onPress={this._onPress}>
          <View style={styles.button}>
            <Text style={styles.buttonText}>Press me!</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: 100,
    height: 100,
    backgroundColor: 'red',
    marginBottom: 10
  },
  box2: {
    width: 5,
    height: 5,
    backgroundColor: 'gray',
  },
  button: {
    backgroundColor: 'black',
    paddingHorizontal: 20,
    paddingVertical: 15,
    marginTop: 15,
  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
  },
});
