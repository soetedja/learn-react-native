import React from 'react';
import { View } from 'react-native';
import List from './dumb_components/List';
import Input from './dumb_components/Input';
import Title from './dumb_components/Title';
import { actionCreators } from './todoListRedux';
import { createStore } from 'redux';

// Import the reducer and create a store
import { reducer } from './todoListRedux';
const store = createStore(reducer);

export default class ReduxScreen extends React.Component {
  static navigationOptions = {
    title: 'Redux ',
  }

  state = {}

  UNSAFE_componentWillMount () {
    const { todos } = store.getState();
    this.setState({ todos });

    this.unsubscribe = store.subscribe(() => {
      const { todos } = store.getState();
      this.setState({ todos });
    });
  }

  componentWillUnmount () {
    this.unsubscribe();
  }
  onAddTodo = (text) => {
    store.dispatch(actionCreators.add(text));
  }

  onRemoveTodo = (index) => {
    store.dispatch(actionCreators.remove(index));
  }

  render () {
    const { todos } = this.state;
    return (
      <View>
        <Title>To Do List</Title>
        <Input
          placeholder={'Type a todo, then hit enter!'}
          onSubmitEditing={this.onAddTodo}
        />
        <List
          list={todos}
          onPressItem={this.onRemoveTodo}
        />
      </View>
    );
  }
}