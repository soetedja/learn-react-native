import React, { Component } from 'react';
import { View } from 'react-native';

import List from './dumb_components/List';
import Input from './dumb_components/Input';
import Title from './dumb_components/Title';

export default class ComponentStateScreen extends Component {
  static navigationOptions = {
    title: 'Component State',
  }

  state = {
    todos: ['Click to remove', 'Learn React Native', 'Write Code', 'Ship App']
  }

  onAddTodo = (text) => {
    const { todos } = this.state;
    this.setState({
      todos: [text, ...todos],
    });
  }

  onRemoveTodo = (index) => {
    const { todos } = this.state;
    this.setState({
      todos: todos.filter((todo, i) => i !== index)
    });
  }

  render () {
    const { todos } = this.state;
    return (
      <View>
        <Title>To-Do List</Title>
        <Input
          placeholder={'Type a todo, then hit enter!'}
          onSubmitEditing={this.onAddTodo}
        />
        <List
          list={todos}
          onPressItem={this.onRemoveTodo}
        />
      </View>
    );
  }
}