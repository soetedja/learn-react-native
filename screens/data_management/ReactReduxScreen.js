import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';

import { actionCreators } from './todoListRedux';
import List from './dumb_components/List';
import Input from './dumb_components/Input';
import Title from './dumb_components/Title';

const mapStateToProps = (state) => ({
  todos: state.todos
});

class ReactReduxScreen extends React.Component {
  static navigationOptions = {
    title: 'React Redux'
  }

  onAddTodo = (text) => {
    const { dispatch } = this.props;
    dispatch(actionCreators.add(text));
  }

  onRemoveTodo = (index) => {
    const { dispatch } = this.props;
    dispatch(actionCreators.remove(index));
  }

  render () {
    const { todos } = this.props;
    return (
      <View>
        <Title>To-Do List</Title>
        <Input
          placeholder={'Type a todo, then hit enter!'}
          onSubmitEditing={this.onAddTodo}
        />
        <List
          list={todos}
          onPressItem={this.onRemoveTodo}
        />
      </View>
    );
  }
}

export default connect(mapStateToProps)(ReactReduxScreen);