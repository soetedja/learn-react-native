import React from 'react';
import { ListView, StyleSheet, Text, TouchableOpacity } from 'react-native';

// Row data (hard-coded)
let id = 1;
const rows = {
  'ES6': [
    { id: id++, text: 'Constant' , navigation: 'Constant'},
    { id: id++, text: 'Scoping' , navigation: 'Scoping'},
    { id: id++, text: 'Arrow Functions' , navigation: 'ArrowFunction'},
    { id: id++, text: 'Extended Parameter Handling' , navigation: 'ExtendedParameterHandling'},
    { id: id++, text: 'Template Literal', navigation: 'TemplateLiteral' },
    { id: id++, text: 'Extended Literal', navigation: 'ExtendedLiteral' },
    { id: id++, text: 'EnhancedRegularExpression', navigation: 'EnhancedRegularExpression' },
    { id: id++, text: 'Enhanced Object Properties', navigation: 'EnhancedObjectProperty' },
    { id: id++, text: 'Destructuring Assignment', navigation: 'DestructuringAssignment' },
    { id: id++, text: 'Module', navigation: 'Module' },
    { id: id++, text: 'Class', navigation: 'Class' },
    { id: id++, text: 'Symbol Type', navigation: 'SymbolType' },
    { id: id++, text: 'Iterator', navigation: 'Iterator' },
    { id: id++, text: 'Generator', navigation: 'Generator' },
    { id: id++, text: 'Map/Set & WeakMap /WeakSet', navigation: 'MapSetAndWeakMapWeakSet' },
    { id: id++, text: 'Typed Arrays', navigation: 'TypedArray' },
    { id: id++, text: 'New Build in Method', navigation: 'NewBuildInMethod' },
    { id: id++, text: 'Promise', navigation: 'Promise' },
    { id: id++, text: 'Meta-Programming', navigation: 'MetaProgramming' },
    { id: id++, text: 'Internationalization & Localization', navigation: 'InternationalizationAndLocalization' },
  ],
  'ES7': [
    { id: id++, text: 'String.prototype.padStart/padEnd' , navigation: 'StringPadStartPadEnd'},
    { id: id++, text: 'Object.entries' , navigation: 'ObjectEntries'},
    { id: id++, text: 'Object.values' , navigation: 'ObjectValues'},
    { id: id++, text: 'Array.prototype.includes' , navigation: 'ArrayIncludes'},
    { id: id++, text: 'Exponentiation' , navigation: 'Exponentiation'},
    { id: id++, text: 'Trailing Commas' , navigation: 'TrailingComma'},
    { id: id++, text: 'async / await' , navigation: 'AsyncAwait'},
  ],
  'Networking': [
    { id: id++, text: 'Fetch' , navigation: 'Fetch'},
    { id: id++, text: 'Axios' , navigation: 'Axios'},
  ]
};

// Row and section comparison functions
const rowHasChanged = (r1, r2) => r1.id !== r2.id;
const sectionHeaderHasChanged = (s1, s2) => s1 !== s2;

// DataSource template object
const ds = new ListView.DataSource({ rowHasChanged, sectionHeaderHasChanged });

export default class CheatSheetScreen extends React.Component {
  static navigationOptions = {
    title: 'Cheat Sheet',
  }
  
  state = {
    dataSource: ds.cloneWithRowsAndSections(rows)
  }

  renderRow = (rowData, sectionId) => {
    if(this.state[sectionId] === true){
      return (
        <TouchableOpacity 
          onPress={() => {
            this.props.navigation.navigate(rowData.navigation);
          }}
          style={styles.rowLink}>
          <Text style={styles.rowLinkText}>{rowData.id}. {rowData.text}</Text>
        </TouchableOpacity>
      );
    } else {
      return <Text style={{height: 1}}></Text>;
    }
  }

  renderSectionHeader = (sectionRows, sectionId) => {
    return (
      <TouchableOpacity 
        onPress={() => {
          let sec = this.state[sectionId] === undefined ? false: this.state[sectionId];
          this.setState({[sectionId]: !sec});
          this.setState({
            dataSource: ds.cloneWithRowsAndSections(rows)
          });
        }}
        style={styles.rowLink}>
        <Text style={styles.header}>
          {sectionId} ({sectionRows.length})
        </Text>
      </TouchableOpacity>
      
    );
  }

  render () {
    return (
      <ListView
        style={styles.container}
        dataSource={this.state.dataSource}
        renderRow={this.renderRow}
        renderSectionHeader={this.renderSectionHeader}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  row: {
    padding: 5,
    paddingLeft: 10,
    marginBottom: 1,
    backgroundColor: 'skyblue',
  },
  header: {
    padding: 5,
    marginBottom: 1,
    backgroundColor: 'steelblue',
    color: 'white',
    fontWeight: 'bold',
  },
  
  rowLink: {
    paddingVertical: 3,
    paddingLeft: 10
  },
  rowLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});