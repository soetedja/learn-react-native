import React from 'react';
import { ScrollView, View, StyleSheet } from 'react-native';
import { CheatSheetContent } from '../CheatSheetContent';

const data = [
  {
    title: 'String.prototype.padStart/padEnd',
    description: 'padStart and padEnd allow us to pad a given string with any text of our choosing to ensure a string matches a given length:',
    code: `
// padStart(desiredLength, textToPrepend)

// No text
''.padStart(10, 'Hi') // 'HiHiHiHiHi'

// Some text
'def'.padStart(6, 'abc') // 'abcdef'

// Only use what gets to length
'5678'.padStart(7, '1234') // '1235678'

// padEnd(desiredLength, textToAppend)

'23'.padEnd(8, '0') // '23000000'
    `,
  },
];

export default class StringPadStartPadEndScreen extends React.Component {
  static navigationOptions = {
    title: 'String.prototype.padStart/padEnd',
  }

  render () {
    return (
      <ScrollView >
        <View style={styles.container}>
          {
            data.map((item, id) => (<CheatSheetContent key={id} id={id + 1} data={item} />)
            )
          }
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  }
});
