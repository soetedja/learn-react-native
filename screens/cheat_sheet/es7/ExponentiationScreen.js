import React from 'react';
import { ScrollView, View, StyleSheet } from 'react-native';
import { CheatSheetContent } from '../CheatSheetContent';

const data = [
  {
    title: 'Array.prototype.includes',
    description: 'avaScript has introduced a shorthand method of exponentiation: This new syntax accomplishes the same as  Math.pow with less code!',
    code: `
// 2 to the power of 8
Math.pow(2, 8) // 256

// ..becomes
2 ** 8 // 256
    `,
  },
];

export default class ExponentiationScreen extends React.Component {
  static navigationOptions = {
    title: 'Array.prototype.includes',
  }

  render () {
    return (
      <ScrollView >
        <View style={styles.container}>
          {
            data.map((item, id) => (<CheatSheetContent key={id} id={id + 1} data={item} />)
            )
          }
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  }
});
