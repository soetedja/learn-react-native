import React from 'react';
import { ScrollView, View, StyleSheet } from 'react-native';
import { CheatSheetContent } from '../CheatSheetContent';

const data = [
  {
    title: 'Anonymous Async Function',
    description: '',
    code: `
let main = (async function() {
  let value = await fetch('/');
})();
    `,
  },
  {
    title: 'Async Function Declaration',
    description: '',
    code: `
async function main() {
  let value = await fetch('/');
};
    `,
  },
  {
    title: 'Async Function Assignment',
    description: '',
    code: `
let main = async function() {
  let value = await fetch('/');
};

// Arrow functions too!
let main = async () => {
  let value = await fetch('/');
};
    `,
  },
  {
    title: 'Async Function as Argument',
    description: '',
    code: `
document.body.addEventListener('click', async function() {
  let value = await fetch('/');
});
    `,
  },
  {
    title: 'Object & Class Methods',
    description: '',
    code: `
// Object property
let obj = {
  async method() {
    let value = await fetch('/');
  }
};

// Class methods
class MyClass {
  async myMethod() {
    let value = await fetch('/');
  }
}
    `,
  },
  {
    title: 'Error Handling',
    description: 'Traditional promise use allows you to use a catch callback to handle rejection.  When you use await, your best bet is using try/catch:',
    code: `
try {
  let x = await myAsyncFunction();
}
catch(e) {
  // Error!
}
    `,
  },
  {
    title: 'Parallelism',
    description: 'The idea is to avoid stacking awaits, when possible, and instead trigger tasks immediately and use await after said tasks are triggered:',
    code: `
// Will take 1000ms total!
async function series() {
  await wait(500);
  await wait(500);
  return "done!";
}

// Would take only 500ms total!
async function parallel() {
  const wait1 = wait(500);
  const wait2 = wait(500);
  await wait1;
  await wait2;
  return "done!";
}
    `,
  },
  {
    title: 'Promise.all Equivalents',
    description: 'Promise API is Promise.all, which fires a callback when all fetches are complete. ',
    code: `
let [foo, bar] = await Promise.all([getFoo(), getBar()]);
    `,
  },
];

export default class AsyncAwaitScreen extends React.Component {
  static navigationOptions = {
    title: 'Async/Await',
  }

  render () {
    return (
      <ScrollView >
        <View style={styles.container}>
          {
            data.map((item, id) => (<CheatSheetContent key={id} id={id + 1} data={item} />)
            )
          }
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  }
});
