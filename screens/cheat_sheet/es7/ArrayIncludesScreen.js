import React from 'react';
import { ScrollView, View, StyleSheet } from 'react-native';
import { CheatSheetContent } from '../CheatSheetContent';
import { GetFunctionBody } from '../../../helper/StringHelper';
const sampleCode = () => {
  return ['a', 'b', 'c'].includes('a'); // true, not 0 like indexOf would give
};
const sampleCode2 = () => {
  return ['a', 'b', 'c'].includes('d'); // false
};

const data = [
  {
    title: 'Array.prototype.includes',
    description: 'Array.prototype.includes is a bit like indexOf but instead returns a true or false value instead of the item\'s index. indexOf has been used over the years to detect item presence in array, but the `0` index can lead to false negatives if not coded properly.  I\'m glad JavaScript has added a function that returns exactly what we need: a positive or negative answer!',
    code: 
`${GetFunctionBody(sampleCode)} -> ${sampleCode()}
${GetFunctionBody(sampleCode2)} -> ${sampleCode2()}`
  },
];

export default class ArrayIncludesScreen extends React.Component {
  static navigationOptions = {
    title: 'Array.prototype.includes',
  }

  render () {
    return (
      <ScrollView >
        <View style={styles.container}>
          {
            data.map((item, id) => (<CheatSheetContent key={id} id={id + 1} data={item} />)
            )
          }
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  }
});
