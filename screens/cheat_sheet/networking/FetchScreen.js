/* eslint-disable no-undef */
import React from 'react';
import { FlatList, ScrollView, View, Text, StyleSheet } from 'react-native';
import { GetFunctionBody } from '../../../helper/StringHelper';
import { MonoText } from '../../../components/StyledText';

const basicFetch = () => {
  return fetch('https://jsonplaceholder.typicode.com/posts')
    .then((response) => response.json())
    .then((responseJson) => {
      return responseJson;
    })
    .catch((error) =>{
      console.error(error);
    });
};

export default class FetchScreen extends React.Component {
  static navigationOptions = {
    title: 'Fetch',
  }

  state = {
    dataSource: []
  }
  
  componentDidMount(){
    basicFetch()
      .then(data => 
      {
        this.setState({
          dataSource: data,
        }, function(){
        });
      }
      ) // JSON-string from `response.json()` call
      .catch(error => console.error(error));
   
  }
  render () {
    return (
      <ScrollView >
        <View style={styles.container}>
          <Text style={styles.title}>Basic Axios</Text>
          <ScrollView horizontal style={styles.scrollView}>
            <MonoText style={styles.code}>{GetFunctionBody(basicFetch, true)}</MonoText>
          </ScrollView>
          <Text style={styles.title}>Result</Text>
          <FlatList
            data={this.state.dataSource}
            renderItem={({item}) => (
              <View>
                <Text style={{fontWeight: 'bold'}}>{item.id}. {item.title}</Text>
                <Text>{item.body}</Text>
              </View>
            )}
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flex: 1,
    flexDirection: 'column',
    alignItems:'stretch',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#005073'
  },
  description: {
    fontWeight: '200',
  },
  scrollView: {
    flex: 1,
    backgroundColor: '#f0f0f0',
    minWidth: '100%',
  },
  code: {
    fontSize: 12,
    padding: 15,
  }
});
