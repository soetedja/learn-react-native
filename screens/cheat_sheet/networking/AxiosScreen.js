/* eslint-disable no-undef */
import React from 'react';
import { ScrollView, Text, View, StyleSheet , FlatList} from 'react-native';
import axios from 'axios';
import { GetFunctionBody } from '../../../helper/StringHelper';
import { MonoText } from '../../../components/StyledText';

export default class AxiosScreen extends React.Component {
  componentDidMount(){
    var that  = this;
    axios.get('https://jsonplaceholder.typicode.com/posts')
      .then(function (response) {
        that.setState({
          dataSource: response.data,
        }, function(){
        });
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .then(function () {
        // always executed
      });
  }
  state = {
    dataSource: []
  }
  
  static navigationOptions = {
    title: 'Axios',
  }

  render () {
    return (
      <ScrollView >
        <View style={styles.container}>
          <Text style={styles.title}>Basic Fetch</Text>
          <ScrollView horizontal style={styles.scrollView}>
            <MonoText style={styles.code}>{GetFunctionBody(this.componentDidMount, true)}</MonoText>
          </ScrollView>
          <Text style={styles.title}>Result</Text>
          <FlatList
            data={this.state.dataSource}
            renderItem={({item}) => (
              <View>
                <Text style={{fontWeight: 'bold'}}>{item.id}. {item.title}</Text>
                <Text>{item.body}</Text>
              </View>
            )}
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flex: 1,
    flexDirection: 'column',
    alignItems:'stretch',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#005073'
  },
  description: {
    fontWeight: '200',
  },
  scrollView: {
    flex: 1,
    backgroundColor: '#f0f0f0',
    minWidth: '100%',
  },
  code: {
    fontSize: 12,
    padding: 15,
  }
});
