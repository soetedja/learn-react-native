import React from 'react';
import { ScrollView, View, Text, StyleSheet } from 'react-native';
import { MonoText } from '../../components/StyledText';
export class CheatSheetContent extends React.Component {
  render () {
    const { id } = this.props;
    const { title, code, description } = this.props.data;
    return (
      <View style={styles.container}>
        <Text style={styles.title}>{id}. {title}</Text>
        <Text style={styles.description}>{description}</Text>
        <ScrollView horizontal style={styles.scrollView}>
          <MonoText style={styles.code}>{code}</MonoText>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flex: 1,
    flexDirection: 'column',
    alignItems:'stretch',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#005073'
  },
  description: {
    fontWeight: '200',
  },
  scrollView: {
    flex: 1,
    backgroundColor: '#f0f0f0',
    minWidth: '100%',
  },
  code: {
    fontSize: 12,
    padding: 15,
  }
});