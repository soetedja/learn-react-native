import React from 'react';
import { ScrollView, View, StyleSheet } from 'react-native';
import { CheatSheetContent } from '../CheatSheetContent';

const data = [
  {
    title: 'Property Shorthand',
    description: 'Shorter syntax for common object property definition idiom.',
    code: `
var x = 0, y = 0
obj = { x, y }
    `,
  },{
    title: 'Computed Property Names',
    description: 'Support for computed names in object property definitions.',
    code: `
let obj = {
  foo: "bar",
  [ "baz" + quux() ]: 42
}
    `,
  },{
    title: 'Method Properties',
    description: 'Support for method notation in object property definitions, for both regular functions and generator functions.',
    code: `
obj = {
  foo (a, b) {
      …
  },
  bar (x, y) {
      …
  },
  *quux (x, y) {
      …
  }
}
    `,
  },
];

export default class EnhancedObjectPropertyScreen extends React.Component {
  static navigationOptions = {
    title: 'Enhanced Regular Expression',
  }

  render () {
    return (
      <ScrollView >
        <View style={styles.container}>
          {
            data.map((item, id) => (<CheatSheetContent key={id} id={id + 1} data={item} />)
            )
          }
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  }
});
