import React from 'react';
import { ScrollView, View, StyleSheet } from 'react-native';
import { CheatSheetContent } from '../CheatSheetContent';

const data = [
  {
    title: 'Regular Expression Sticky Matching',
    description: 'Keep the matching position sticky between matches and this way support efficient parsing of arbitrary long input strings, even with an arbitrary number of distinct regular expressions.',
    code: `
let parser = (input, match) => {
  for (let pos = 0, lastPos = input.length; pos < lastPos; ) {
      for (let i = 0; i < match.length; i++) {
          match[i].pattern.lastIndex = pos
          let found
          if ((found = match[i].pattern.exec(input)) !== null) {
              match[i].action(found)
              pos = match[i].pattern.lastIndex
              break
          }
      }
  }
}

let report = (match) => {
  console.log(JSON.stringify(match))
}
parser("Foo 1 Bar 7 Baz 42", [
  { pattern: /Foo\\s+(\\d+)/y, action: (match) => report(match) },
  { pattern: /Bar\\s+(\\d+)/y, action: (match) => report(match) },
  { pattern: /Baz\\s+(\\d+)/y, action: (match) => report(match) },
  { pattern: /\\s*/y,         action: (match) => {}            }
])
    `,
  },
];

export default class EnhancedRegularExpressionScreen extends React.Component {
  static navigationOptions = {
    title: 'Enhanced Regular Expression',
  }

  render () {
    return (
      <ScrollView >
        <View style={styles.container}>
          {
            data.map((item, id) => (<CheatSheetContent key={id} id={id + 1} data={item} />)
            )
          }
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  }
});
