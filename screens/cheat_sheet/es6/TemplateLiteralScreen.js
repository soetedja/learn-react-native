import React from 'react';
import { ScrollView, View, StyleSheet } from 'react-native';
import { CheatSheetContent } from '../CheatSheetContent';

const data = [
  {
    title: 'String Interpolation',
    description: 'Intuitive expression interpolation for single-line and multi-line strings. (Notice: don\'t be confused, Template Literals were originally named "Template Strings" in the drafts of the ECMAScript 6 language specification)',
    code: `
var customer = { name: "Foo" }
var card = { amount: 7, product: "Bar", unitprice: 42 }
var message = \`Hello \${customer.name},
want to buy \${card.amount} \${card.product} for
a total of \${card.amount * card.unitprice} bucks?\`
    `,
  },
  {
    title: 'Custom Interpolation',
    description: 'Flexible expression interpolation for arbitrary methods.',
    code: `
get\`http://example.com/foo?bar=\${bar + baz}&quux=\${quux}\`
   `,
  },{
    title: 'Raw String Access',
    description: 'Access the raw template string content (backslashes are not interpreted).',
    code: `
function quux (strings, ...values) {
  strings[0] === "foo\n"
  strings[1] === "bar"
  strings.raw[0] === "foo\\n"
  strings.raw[1] === "bar"
  values[0] === 42
}
quux\`foo\\n\${ 42 }bar\`

String.raw\`foo\n\${ 42 }bar\` === "foo\\n42bar"
   `
  },
];

export default class TemplateLiteralScreen extends React.Component {
  static navigationOptions = {
    title: 'Template Literals',
  }

  render () {
    return (
      <ScrollView >
        <View style={styles.container}>
          {
            data.map((item, id) => (<CheatSheetContent key={id} id={id + 1} data={item} />)
            )
          }
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  }
});
