import React from 'react';
import { ScrollView, View, StyleSheet } from 'react-native';
import { CheatSheetContent } from '../CheatSheetContent';

const data = [
  {
    title: 'Set Data-Structure',
    description: 'Cleaner data-structure for common algorithms based on sets.',
    code: `
let s = new Set()
s.add("hello").add("goodbye").add("hello")
s.size === 2
s.has("hello") === true
for (let key of s.values()) // insertion order
  console.log(key)
    `,
  },
  {
    title: 'Map Data-Structure',
    description: 'Cleaner data-structure for common algorithms based on maps.',
    code: `
let m = new Map()
let s = Symbol()
m.set("hello", 42)
m.set(s, 34)
m.get(s) === 34
m.size === 2
for (let [ key, val ] of m.entries())
  console.log(key + " = " + val)
   `,
  },
  {
    title: 'Weak-Link Data-Structures',
    description: 'Memory-leak-free Object-key’d side-by-side data-structures.',
    code: `
let isMarked     = new WeakSet()
let attachedData = new WeakMap()

export class Node {
  constructor (id)   { this.id = id                  }
  mark        ()     { isMarked.add(this)            }
  unmark      ()     { isMarked.delete(this)         }
  marked      ()     { return isMarked.has(this)     }
  set data    (data) { attachedData.set(this, data)  }
  get data    ()     { return attachedData.get(this) }
}

let foo = new Node("foo")

JSON.stringify(foo) === '{"id":"foo"}'
foo.mark()
foo.data = "bar"
foo.data === "bar"
JSON.stringify(foo) === '{"id":"foo"}'

isMarked.has(foo)     === true
attachedData.has(foo) === true
foo = null  /* remove only reference to foo */
attachedData.has(foo) === false
isMarked.has(foo)     === false
    `,
  }
];

export default class MapSetAndWeakMapWeakSetScreen extends React.Component {
  static navigationOptions = {
    title: 'Map/Set & WeakMap/WeakSet',
  }

  render () {
    return (
      <ScrollView >
        <View style={styles.container}>
          {
            data.map((item, id) => (<CheatSheetContent key={id} id={id + 1} data={item} />)
            )
          }
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  }
});
