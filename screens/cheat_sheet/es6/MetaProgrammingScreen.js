import React from 'react';
import { ScrollView, View, StyleSheet } from 'react-native';
import { CheatSheetContent } from '../CheatSheetContent';

const data = [
  {
    title: 'Proxying',
    description: 'Hooking into runtime-level object meta-operations.',
    code: `
let target = {
  foo: "Welcome, foo"
}
let proxy = new Proxy(target, {
  get (receiver, name) {
      return name in receiver ? receiver[name] : \`Hello, \${name}\`
  }
})
proxy.foo   === "Welcome, foo"
proxy.world === "Hello, world"
    `,
  },
  {
    title: 'Reflection',
    description: 'Make calls corresponding to the object meta-operations.',
    code: `
let obj = { a: 1 }
Object.defineProperty(obj, "b", { value: 2 })
obj[Symbol("c")] = 3
Reflect.ownKeys(obj) // [ "a", "b", Symbol(c) ]
    `,
  }
];

export default class MetaProgrammingScreen extends React.Component {
  static navigationOptions = {
    title: 'Meta Programming',
  }

  render () {
    return (
      <ScrollView >
        <View style={styles.container}>
          {
            data.map((item, id) => (<CheatSheetContent key={id} id={id + 1} data={item} />)
            )
          }
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  }
});
