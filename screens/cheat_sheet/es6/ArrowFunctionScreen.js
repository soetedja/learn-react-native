import React from 'react';
import { ScrollView, View, StyleSheet } from 'react-native';
import { CheatSheetContent } from '../CheatSheetContent';

const data = [
  {
    title: 'Expression Bodies',
    description: 'More expressive closure syntax.',
    code: `
odds  = evens.map(v => v + 1)
pairs = evens.map(v => ({ even: v, odd: v + 1 }))
nums  = evens.map((v, i) => v + i)
    `,
  },
  {
    title: 'Statement Bodies',
    description: 'More expressive closure syntax.',
    code: `
nums.forEach(v => {
  if (v % 5 === 0)
      fives.push(v)
})
   `,
  },
  {
    title: 'Lexical this',
    description: 'More intuitive handling of current object context.',
    code: `
this.nums.forEach((v) => {
  if (v % 5 === 0)
      this.fives.push(v)
})
    `,
  }
];

export default class ArrowFunctionScreen extends React.Component {
  static navigationOptions = {
    title: 'Arrow Functions',
  }

  render () {
    return (
      <ScrollView >
        <View style={styles.container}>
          {
            data.map((item, id) => (<CheatSheetContent key={id} id={id + 1} data={item} />)
            )
          }
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  }
});
