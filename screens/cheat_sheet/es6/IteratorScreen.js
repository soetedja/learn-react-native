import React from 'react';
import { ScrollView, View, StyleSheet } from 'react-native';
import { CheatSheetContent } from '../CheatSheetContent';

const data = [
  {
    title: 'Iterator & For-Of Operator',
    description: 'Support "iterable" protocol to allow objects to customize their iteration behaviour. Additionally, support "iterator" protocol to produce sequence of values (either finite or infinite). Finally, provide convenient of operator to iterate over all values of an iterable object.',
    code: `
let fibonacci = {
  [Symbol.iterator]() {
      let pre = 0, cur = 1
      return {
          next () {
              [ pre, cur ] = [ cur, pre + cur ]
              return { done: false, value: cur }
          }
      }
  }
}

for (let n of fibonacci) {
  if (n > 1000)
      break
  console.log(n)
}
    `,
  }
];

export default class IteratorScreen extends React.Component {
  static navigationOptions = {
    title: 'Iterators',
  }

  render () {
    return (
      <ScrollView >
        <View style={styles.container}>
          {
            data.map((item, id) => (<CheatSheetContent key={id} id={id + 1} data={item} />)
            )
          }
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  }
});
