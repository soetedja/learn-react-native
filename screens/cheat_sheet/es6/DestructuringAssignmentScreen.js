import React from 'react';
import { ScrollView, View, StyleSheet } from 'react-native';
import { CheatSheetContent } from '../CheatSheetContent';

const data = [
  {
    title: 'Array Matching',
    description: 'Intuitive and flexible destructuring of Arrays into individual variables during assignment.',
    code: `
var list = [ 1, 2, 3 ]
var [ a, , b ] = list
[ b, a ] = [ a, b ]
    `,
  },
  {
    title: 'Computed Property Names',
    description: 'Support for computed names in object property definitions.',
    code: `
let obj = {
  foo: "bar",
  [ "baz" + quux() ]: 42
}
    `,
  },
  {
    title: 'Object Matching, Shorthand Notation',
    description: 'Intuitive and flexible destructuring of Objects into individual variables during assignment.',
    code: `
var { op, lhs, rhs } = getASTNode()
    `,
  },
  {
    title: 'Object Matching, Deep Matching',
    description: 'Intuitive and flexible destructuring of Objects into individual variables during assignment.',
    code: `
var { op: a, lhs: { op: b }, rhs: c } = getASTNode()
    `,
  },
  {
    title: 'Object And Array Matching, Default Values',
    description: 'Simple and intuitive default values for destructuring of Objects and Arrays.',
    code: `
var obj = { a: 1 }
var list = [ 1 ]
var { a, b = 2 } = obj
var [ x, y = 2 ] = list
    `,
  },
  {
    title: 'Parameter Context Matching',
    description: 'Intuitive and flexible destructuring of Arrays and Objects into individual parameters during function calls.',
    code: `
function f ([ name, val ]) {
  console.log(name, val)
}
function g ({ name: n, val: v }) {
    console.log(n, v)
}
function h ({ name, val }) {
    console.log(name, val)
}
f([ "bar", 42 ])
g({ name: "foo", val:  7 })
h({ name: "bar", val: 42 })
    `,
  },
  {
    title: 'Fail-Soft Destructuring',
    description: 'Fail-soft destructuring, optionally with defaults.',
    code: `
var list = [ 7, 42 ]
var [ a = 1, b = 2, c = 3, d ] = list
a === 7
b === 42
c === 3
d === undefined
    `,
  },
];

export default class DestructuringAssignmentScreen extends React.Component {
  static navigationOptions = {
    title: 'Destructuring Assignment',
  }

  render () {
    return (
      <ScrollView >
        <View style={styles.container}>
          {
            data.map((item, id) => (<CheatSheetContent key={id} id={id + 1} data={item} />)
            )
          }
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  }
});
