import React from 'react';
import { ScrollView, View, StyleSheet } from 'react-native';
import { CheatSheetContent } from '../CheatSheetContent';

const data = [
  {
    title: 'Binary & Octal Literal',
    description: 'Direct support for safe binary and octal literals.',
    code: `
0b111110111 === 503
0o767 === 503
    `,
  },
  {
    title: 'Unicode String & RegExp Literal',
    description: 'Extended support using Unicode within strings and regular expressions.',
    code: `
"𠮷".length === 2
"𠮷".match(/./u)[0].length === 2
"𠮷" === "\uD842\uDFB7"
"𠮷" === "\u{20BB7}"
"𠮷".codePointAt(0) == 0x20BB7
for (let codepoint of "𠮷") console.log(codepoint)
   `,
  },
];

export default class ExtendedLiteralScreen extends React.Component {
  static navigationOptions = {
    title: 'Extended Literal',
  }

  render () {
    return (
      <ScrollView >
        <View style={styles.container}>
          {
            data.map((item, id) => (<CheatSheetContent key={id} id={id + 1} data={item} />)
            )
          }
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  }
});
