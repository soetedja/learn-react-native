import React from 'react';
import { ScrollView, View, StyleSheet } from 'react-native';
import { CheatSheetContent } from '../CheatSheetContent';

const data = [
  {
    title: 'Default Parameter Values',
    description: 'Simple and intuitive default values for function parameters.',
    code: `
function f (x, y = 7, z = 42) {
  return x + y + z
}
f(1) === 50`,
  },
  {
    title: 'Rest Parameter',
    description: 'Aggregation of remaining arguments into single parameter of variadic functions.',
    code: `
function f (x, y, ...a) {
  return (x + y) * a.length
}
f(1, 2, "hello", true, 7) === 9`,
  },
  {
    title: 'Spread Operator',
    description: 'Spreading of elements of an iterable collection (like an array or even a string) into both literal elements and individual function parameters.',
    code: `
var params = [ "hello", true, 7 ]
var other = [ 1, 2, ...params ] 
// [ 1, 2, "hello", true, 7 ]

function f (x, y, ...a) {
    return (x + y) * a.length
}
f(1, 2, ...params) === 9

var str = "foo"
var chars = [ ...str ] 
// [ "f", "o", "o" ]`,
  }
];

export default class ExtendedParameterHandlingScreen extends React.Component {
  static navigationOptions = {
    title: 'Extended Parameter HandlingScreen',
  }

  render () {
    return (
      <ScrollView >
        <View style={styles.container}>
          {
            data.map((item, id) => (<CheatSheetContent key={id} id={id+1} data={item} />)
            )
          }
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  }
});
