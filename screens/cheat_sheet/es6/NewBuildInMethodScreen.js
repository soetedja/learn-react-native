import React from 'react';
import { ScrollView, View, StyleSheet } from 'react-native';
import { CheatSheetContent } from '../CheatSheetContent';

const data = [
  {
    title: 'Object Property Assignment',
    description: 'New function for assigning enumerable properties of one or more source objects onto a destination object.',
    code: `
var dest = { quux: 0 }
var src1 = { foo: 1, bar: 2 }
var src2 = { foo: 3, baz: 4 }
Object.assign(dest, src1, src2)
dest.quux === 0
dest.foo  === 3
dest.bar  === 2
dest.baz  === 4
    `,
  },
  {
    title: 'Array Element Finding',
    description: 'New function for finding an element in an array.',
    code: `
[ 1, 3, 4, 2 ].find(x => x > 3) // 4
[ 1, 3, 4, 2 ].findIndex(x => x > 3) // 2
   `,
  },
  {
    title: 'String Repeating',
    description: 'New string repeating functionality.',
    code: `
" ".repeat(4 * depth)
"foo".repeat(3)
    `,
  },
  {
    title: 'String Searching',
    description: 'New specific string functions to search for a sub-string.',
    code: `
"hello".startsWith("ello", 1) // true
"hello".endsWith("hell", 4)   // true
"hello".includes("ell")       // true
"hello".includes("ell", 1)    // true
"hello".includes("ell", 2)    // false
    `,
  },
  {
    title: 'Number Type Checking',
    description: 'New functions for checking for non-numbers and finite numbers. for a sub-string.',
    code: `
Number.isNaN(42) === false
Number.isNaN(NaN) === true

Number.isFinite(Infinity) === false
Number.isFinite(-Infinity) === false
Number.isFinite(NaN) === false
Number.isFinite(123) === true
    `,
  },
  {
    title: 'Number Safety Checking',
    description: 'Checking whether an integer number is in the safe range, i.e., it is correctly represented by JavaScript (where all numbers, including integer numbers, are technically floating point number).',
    code: `
Number.isSafeInteger(42) === true
Number.isSafeInteger(9007199254740992) === false
    `,
  },
  {
    title: 'Number Comparison',
    description: 'Availability of a standard Epsilon value for more precise comparison of floating point numbers.',
    code: `
console.log(0.1 + 0.2 === 0.3) // false
console.log(Math.abs((0.1 + 0.2) - 0.3) < Number.EPSILON) // true
    `,
  },
  {
    title: 'Number Truncation',
    description: 'Truncate a floating point number to its integral part, completely dropping the fractional part.',
    code: `
console.log(Math.trunc(42.7)) // 42
console.log(Math.trunc( 0.1)) // 0
console.log(Math.trunc(-0.1)) // -0
    `,
  },
  {
    title: 'Number Sign Determination',
    description: 'Determine the sign of a number, including special cases of signed zero and non-number.',
    code: `
console.log(Math.sign(7))   // 1
console.log(Math.sign(0))   // 0
console.log(Math.sign(-0))  // -0
console.log(Math.sign(-7))  // -1
console.log(Math.sign(NaN)) // NaN
    `,
  }
];

export default class NewBuildInMethodScreen extends React.Component {
  static navigationOptions = {
    title: 'New Built-In Methods',
  }

  render () {
    return (
      <ScrollView >
        <View style={styles.container}>
          {
            data.map((item, id) => (<CheatSheetContent key={id} id={id + 1} data={item} />)
            )
          }
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  }
});
