import React from 'react';
import { ListView, StyleSheet, Text, TouchableOpacity } from 'react-native';

// Row data (hard-coded)
const rows = [
  { id: 1, text: 'Component State' , navigation: 'ComponentState'},
  { id: 2.1, text: 'Minimal Redux', navigation: 'MinimalRedux' },
  { id: 2.2, text: 'Redux', navigation: 'Redux' },
  { id: 2.3, text: 'React Redux' , navigation: 'ReactRedux'},
  { id: 3, text: 'GraphQL' , navigation: 'GraphQL'},
  { id: 4, text: 'Realm', navigation: 'Realm' },
];


// Row and section comparison functions
const rowHasChanged = (r1, r2) => r1.id !== r2.id;

// DataSource template object
const ds = new ListView.DataSource({ rowHasChanged });

export default class DataMangementScreen extends React.Component {
  static navigationOptions = {
    title: 'Data Management',
  }

  state = {
    dataSource: ds.cloneWithRows(rows)
  }

  renderRow = (rowData) => {
    return (
      <TouchableOpacity 
        onPress={() => {
          this.props.navigation.navigate(rowData.navigation);
        }}
        style={styles.rowLink}>
        <Text style={styles.rowLinkText}>{rowData.id}. {rowData.text}</Text>
      </TouchableOpacity>
      // <Button 
      //   style={styles.row} 
      //   title={rowData.text}
      //   onPress={() => {
      //     this.props.navigation.navigate('Home');
      //   }}
      // >
      // </Button>
    );
  }

  render () {
    return (
      <ListView
        style={styles.container}
        dataSource={this.state.dataSource}
        renderRow={this.renderRow}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  row: {
    padding: 5,
    paddingLeft: 10,
    marginBottom: 1,
    backgroundColor: 'skyblue',
  },
  header: {
    padding: 5,
    marginBottom: 1,
    backgroundColor: 'steelblue',
    color: 'white',
    fontWeight: 'bold',
  },
  
  rowLink: {
    paddingVertical: 3,
    paddingLeft: 10
  },
  rowLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});