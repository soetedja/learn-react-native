import React from 'react';
import { ListView, StyleSheet, Text, TouchableOpacity } from 'react-native';

// Row data (hard-coded)
const rows = {
  'Persistence': [
    { id: 1.1, text: 'AsyncStorage' , navigation: 'AsyncStorage'},
    { id: 1.2, text: 'Redux Persist', navigation: 'ReduxPersist' },
  ],
  'Networking': [
    { id: 2, text: 'With Redux' , navigation: 'WithRedux'},
  ],
  'Animation': [
    { id: 3.1, text: 'Animated' , navigation: 'Animated'},
    { id: 3.2, text: 'More Animated' , navigation: 'MoreAnimated'},
    { id: 3.3, text: 'Layout Animation' , navigation: 'LayoutAnimation'},
    { id: 3.4, text: 'RN Animatable' , navigation: 'RNAnimatable'},
    { id: 3.5, text: 'Gestures' , navigation: 'Gestures'},
    { id: 3.6, text: 'Ball' , navigation: 'Ball'},
    { id: 3.6, text: 'Swipe Card' , navigation: 'SwipeCard'},
  ],
  'Boilerplates': [
    { id: 4, text: 'Boilerplates' , navigation: 'Boilerplates'},
  ],
};

// Row and section comparison functions
const rowHasChanged = (r1, r2) => r1.id !== r2.id;
const sectionHeaderHasChanged = (s1, s2) => s1 !== s2;

// DataSource template object
const ds = new ListView.DataSource({ rowHasChanged, sectionHeaderHasChanged });

export default class OtherScreen extends React.Component {
  static navigationOptions = {
    title: 'Other',
  }

  state = {
    dataSource: ds.cloneWithRowsAndSections(rows)
  }

  renderRow = (rowData) => {
    return (
      <TouchableOpacity 
        onPress={() => {
          this.props.navigation.navigate(rowData.navigation);
        }}
        style={styles.rowLink}>
        <Text style={styles.rowLinkText}>{rowData.id}. {rowData.text}</Text>
      </TouchableOpacity>
      // <Button 
      //   style={styles.row} 
      //   title={rowData.text}
      //   onPress={() => {
      //     this.props.navigation.navigate('Home');
      //   }}
      // >
      // </Button>
    );
  }

  renderSectionHeader = (sectionRows, sectionId) => {
    return (
      <Text style={styles.header}>
        {sectionId} ({sectionRows.length})
      </Text>
    );
  }

  render () {
    return (
      <ListView
        style={styles.container}
        dataSource={this.state.dataSource}
        renderRow={this.renderRow}
        renderSectionHeader={this.renderSectionHeader}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  row: {
    padding: 5,
    paddingLeft: 10,
    marginBottom: 1,
    backgroundColor: 'skyblue',
  },
  header: {
    padding: 5,
    marginBottom: 1,
    backgroundColor: 'steelblue',
    color: 'white',
    fontWeight: 'bold',
  },
  
  rowLink: {
    paddingVertical: 3,
    paddingLeft: 10
  },
  rowLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});