export function GetFunctionBody(fn, keepReturn) {
  let fnString = fn.toString();
  var fnbody = fnString.slice(fnString.indexOf('{') + 1, fnString.lastIndexOf('}'));
  if (!keepReturn) fnbody = fnbody.replace('return ', '');

  return fnbody.replace(/^ +/gm, '').trim();
}