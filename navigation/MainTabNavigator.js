import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';

// Components
import CoreComponentScreen from '../screens/CoreComponentScreen';
import ViewScreen from '../screens/core_component/ViewScreen';
import FlexboxScreen from '../screens/core_component/FlexboxScreen';
import TextScreen from '../screens/core_component/TextScreen';
import ImageScreen from '../screens/core_component/ImageScreen';
import ScrollViewScreen from '../screens/core_component/ScrollViewScreen';
import FlatListScreen from '../screens/core_component/FlatListScreen';
import HomogenousSectionListScreen from '../screens/core_component/HomogenousSectionListScreen';
import HeterogenousSectionListScreen from '../screens/core_component/HeterogenousSectionListScreen';
import ListViewScreen from '../screens/core_component/ListViewScreen';

// Data Management
import DataMangementScreen from '../screens/DataMangementScreen';
import ComponentStateScreen from '../screens/data_management/ComponentStateScreen';
import MinimalReduxScreen from '../screens/data_management/MinimalReduxScreen';
import ReduxScreen from '../screens/data_management/ReduxScreen';
import ReactReduxScreen from '../screens/data_management/ReactReduxScreen';
import GraphQLScreen from '../screens/data_management/GraphQLScreen';
import RealmScreen from '../screens/data_management/RealmScreen';

// Other
import OtherScreen from '../screens/OtherScreen';
import AnimatedScreen from '../screens/other/animation/AnimatedScreen';
import RNAnimatableScreen from '../screens/other/animation/RNAnimatableScreen';
import GesturesScreen from '../screens/other/animation/GesturesScreen';
import LayoutAnimationScreen from '../screens/other/animation/LayoutAnimationScreen';
import MoreAnimatedScreen from '../screens/other/animation/MoreAnimatedScreen';
import BallScreen from '../screens/other/animation/BallScreen';
import SwipeCardScreen from '../screens/other/animation/SwipeCardScreen';

// Cheat sheet
import CheatSheetScreen from '../screens/CheatSheetScreen';
import ExtendedParameterHanldingScreen from '../screens/cheat_sheet/es6/ExtendedParameterHandlingScreen';
import ConstantScreen from '../screens/cheat_sheet/es6/ConstantScreen';
import ScopingScreen from '../screens/cheat_sheet/es6/ScopingScreen';
import ArrowFunctionScreen from '../screens/cheat_sheet/es6/ArrowFunctionScreen';
import TemplateLiteralScreen from '../screens/cheat_sheet/es6/TemplateLiteralScreen';
import ExtendedLiteralScreen from '../screens/cheat_sheet/es6/ExtendedLiteralScreen';
import EnhancedRegularExpressionScreen from '../screens/cheat_sheet/es6/EnhancedRegularExpressionScreen';
import EnhancedObjectPropertyScreen from '../screens/cheat_sheet/es6/EnhancedObjectPropertyScreen';
import DestructuringAssignmentScreen from '../screens/cheat_sheet/es6/DestructuringAssignmentScreen';
import ModuleScreen from '../screens/cheat_sheet/es6/ModuleScreen';
import ClassScreen from '../screens/cheat_sheet/es6/ClassScreen';
import SymbolTypeScreen from '../screens/cheat_sheet/es6/SymbolTypeScreen';
import IteratorScreen from '../screens/cheat_sheet/es6/IteratorScreen';
import GeneratorScreen from '../screens/cheat_sheet/es6/GeneratorScreen';
import MapSetAndWeakMapWeakSetScreen from '../screens/cheat_sheet/es6/MapSetAndWeakMapWeakSetScreen';
import TypedArrayScreen from '../screens/cheat_sheet/es6/TypedArrayScreen';
import NewBuildInMethodScreen from '../screens/cheat_sheet/es6/NewBuildInMethodScreen';
import PromiseScreen from '../screens/cheat_sheet/es6/PromiseScreen';
import MetaProgrammingScreen from '../screens/cheat_sheet/es6/MetaProgrammingScreen';
import InternationalizationAndLocalizationScreen from '../screens/cheat_sheet/es6/InternationalizationAndLocalizationScreen';

import ArrayIncludesScreen from '../screens/cheat_sheet/es7/ArrayIncludesScreen';
import AsyncAwaitScreen from '../screens/cheat_sheet/es7/AsyncAwaitScreen';
import ExponentiationScreen from '../screens/cheat_sheet/es7/ExponentiationScreen';
import ObjectEntriesScreen from '../screens/cheat_sheet/es7/ObjectEntriesScreen';
import ObjectValuesScreen from '../screens/cheat_sheet/es7/ObjectValuesScreen';
import StringPadStartPadEndScreen from '../screens/cheat_sheet/es7/StringPadStartPadEndScreen';
import TrailingCommaScreen from '../screens/cheat_sheet/es7/TrailingCommaScreen';

import FetchScreen from '../screens/cheat_sheet/networking/FetchScreen';
import AxiosScreen from '../screens/cheat_sheet/networking/AxiosScreen';

import { createStore } from 'redux';
import { Provider } from 'react-redux';

// Import the reducer and create a store
import { reducer } from '../screens/data_management/todoListRedux';
const store = createStore(reducer);

const HomeStack = createStackNavigator({
  Home: HomeScreen
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={'md-home'}
    />
  ),
};

const ComponentStack = createStackNavigator({
  Component: CoreComponentScreen,
  View: ViewScreen,
  Flexbox: FlexboxScreen,
  Text: TextScreen,
  Image: ImageScreen,
  ScrollView: ScrollViewScreen,
  FlatList: FlatListScreen,
  HomogenousSectionList: HomogenousSectionListScreen,
  HeterogenousSectionList: HeterogenousSectionListScreen,
  ListView: ListViewScreen,
});

ComponentStack.navigationOptions = {
  tabBarLabel: 'Component',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-apps${focused ? '' : ''}`
          : 'md-apps'
      }
    />
  ),
};

const ReactReduxScreenWithStore = () => (
  <Provider store={store}>
    <ReactReduxScreen />
  </Provider>
);

const DataManagementStack = createStackNavigator({
  DataMangement: DataMangementScreen,
  ComponentState: ComponentStateScreen,
  MinimalRedux: MinimalReduxScreen,
  Redux: ReduxScreen,
  ReactRedux: ReactReduxScreenWithStore,
  GraphQL: GraphQLScreen,
  Realm: RealmScreen,
});

DataManagementStack.navigationOptions = {
  tabBarLabel: 'Data Management',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios' ? 'ios-cube' : 'md-cube'
      }
    />
  ),
};

const OtherStack = createStackNavigator({
  Other: OtherScreen,
  Animated: AnimatedScreen,
  RNAnimatable: RNAnimatableScreen,
  Gestures: GesturesScreen,
  LayoutAnimation: LayoutAnimationScreen,
  MoreAnimated: MoreAnimatedScreen,
  Ball: BallScreen,
  SwipeCard: SwipeCardScreen
});

OtherStack.navigationOptions = {
  tabBarLabel: 'Other',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios' ? 'ios-menu' : 'md-menu'
      }
    />
  ),
};

const CheatSheetStack = createStackNavigator({
  CheatSheet: CheatSheetScreen,
  Constant: ConstantScreen,
  ExtendedParameterHandling: ExtendedParameterHanldingScreen,
  TemplateLiteral: TemplateLiteralScreen,
  ArrowFunction: ArrowFunctionScreen,
  Scoping: ScopingScreen,
  ExtendedLiteral: ExtendedLiteralScreen,
  EnhancedRegularExpression: EnhancedRegularExpressionScreen,
  EnhancedObjectProperty: EnhancedObjectPropertyScreen,
  DestructuringAssignment: DestructuringAssignmentScreen,
  Module: ModuleScreen,
  Class: ClassScreen,
  SymbolType: SymbolTypeScreen,
  Iterator: IteratorScreen,
  Generator: GeneratorScreen,
  MapSetAndWeakMapWeakSet: MapSetAndWeakMapWeakSetScreen,
  TypedArray: TypedArrayScreen,
  NewBuildInMethod: NewBuildInMethodScreen,
  Promise: PromiseScreen,
  MetaProgramming: MetaProgrammingScreen,
  InternationalizationAndLocalization: InternationalizationAndLocalizationScreen,
  ArrayIncludes: ArrayIncludesScreen,
  AsyncAwait: AsyncAwaitScreen,
  Exponentiation: ExponentiationScreen,
  ObjectEntries: ObjectEntriesScreen,
  ObjectValues: ObjectValuesScreen,
  StringPadStartPadEnd: StringPadStartPadEndScreen,
  TrailingComma: TrailingCommaScreen,
  Fetch: FetchScreen,
  Axios: AxiosScreen,
});

CheatSheetStack.navigationOptions = {
  tabBarLabel: 'Cheat Sheet',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios' ? 'ios-clipboard' : 'md-clipboard'
      }
    />
  ),
};
export default createBottomTabNavigator({
  HomeStack,
  ComponentStack,
  DataManagementStack,
  CheatSheetStack,
  OtherStack,
});
